#!/usr/bin/env python

import sys
import socket
import re
import dns.resolver

exp = re.compile('([^\.]+)')

hosts = []

if __name__ == '__main__':


   for host in sys.stdin:
      host = host.replace('\n', '')
      if host == "" or host[0] == "#":
          print(host)
          continue

      if host[0] == '!':
         print(host[1:])
         continue

      try:
         if True:
            res = dns.resolver.query(host)
            ips = socket.getaddrinfo(str(res.qname), None)

            for ip in ips:
               entry = (ip[4][0], str(res.qname)[:-1], exp.match(host).group(0))
               if not entry in hosts:
                  print("{0:45} {1} {2}".format(ip[4][0], str(res.qname)[:-1], exp.match(host).group(0)))
                  hosts.append(entry)
      except:
         sys.stderr.write('Failed to get IP for: {0}\n'.format(host))
